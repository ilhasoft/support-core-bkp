package br.com.ilhasoft.support.core.widgets;

import android.database.Observable;
import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by daniel on 02/10/15.
 */
public abstract class EmptyRecyclerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected final PlusObservable<OnIsEmptyChangeListener> onIsEmptyChangeListeners = new PlusObservable<>();

    public EmptyRecyclerAdapter() {
        this.registerAdapterDataObserver(adapterDataObserver);
    }

    public void addOnIsEmptyChangeListeners(OnIsEmptyChangeListener listener) {
        onIsEmptyChangeListeners.registerObserver(listener);
    }

    public void removeOnIsEmptyChangeListener(OnIsEmptyChangeListener onIsEmptyChangeListener) {
        onIsEmptyChangeListeners.unregisterObserver(onIsEmptyChangeListener);
    }

    private void checkIfIsEmpty() {
        final boolean hasChanged = (this.getItemCount() == 0);
        for (OnIsEmptyChangeListener listener : onIsEmptyChangeListeners.getObservers()) {
            listener.onChanged(hasChanged);
        }
    }

    private final RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            checkIfIsEmpty();
        }
        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            checkIfIsEmpty();
        }
        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            checkIfIsEmpty();
        }
    };

    public interface OnIsEmptyChangeListener {
        void onChanged(boolean isEmpty);
    }

    public static class PlusObservable<T> extends Observable<T> {
        public boolean hasObservers() {
            return !mObservers.isEmpty();
        }
        public List<T> getObservers() {
            return mObservers;
        }
    }

}
