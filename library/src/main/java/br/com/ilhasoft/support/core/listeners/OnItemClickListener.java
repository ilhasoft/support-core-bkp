package br.com.ilhasoft.support.core.listeners;

/**
 * Created by danielsan on 5/9/16.
 */
public interface OnItemClickListener<T> {

    void onItemClick(T t, int postion);

}
