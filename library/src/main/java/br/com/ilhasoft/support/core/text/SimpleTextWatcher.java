package br.com.ilhasoft.support.core.text;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by daniel on 05/10/15.
 */
public class SimpleTextWatcher implements TextWatcher {

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) { }

    @Override
    public void afterTextChanged(Editable s) { }

}
