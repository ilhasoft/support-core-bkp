package br.com.ilhasoft.support.core.widgets;

import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by johncordeiro on 7/15/15.
 */
public class SpaceItemDecoration extends BaseItemDecoration {

    private int space;

    public SpaceItemDecoration(@Orientation int orientation) {
        super(orientation);
    }

    public SpaceItemDecoration(@Orientation int orientation, int space) {
        super(orientation);
        this.space = space;
    }

    public int getSpace() {
        return space;
    }

    public void setSpace(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (orientation == LinearLayoutManager.VERTICAL) {
            outRect.bottom = space;
        } else {
            outRect.right = space;
        }
    }
}
