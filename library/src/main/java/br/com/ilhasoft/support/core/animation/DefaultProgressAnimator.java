package br.com.ilhasoft.support.core.animation;

import android.animation.Animator;
import android.animation.ValueAnimator;

/**
 * Created by daniel on 29/11/15.
 */
public class DefaultProgressAnimator extends ProgressAnimator {

    private final ValueAnimator valueAnimator;

    public DefaultProgressAnimator(final ProgressAnimatorListener progressAnimatorListener) {
        super(progressAnimatorListener);
        valueAnimator = new ValueAnimator();
    }

    @Override
    public void start() {
        if (!valueAnimator.isRunning()) {
            valueAnimator.setFloatValues(100, 0);
            valueAnimator.setDuration(duration);
            valueAnimator.addUpdateListener(animatorUpdateListener);
            valueAnimator.addListener(endAnimatorListener);
            valueAnimator.start();
        }
    }

    @Override
    public void stop() {
        if (valueAnimator.isRunning()) {
            valueAnimator.cancel();
        }
    }

    @Override
    public boolean isRunning() {
        return valueAnimator.isRunning();
    }

    private final ValueAnimator.AnimatorUpdateListener animatorUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            if (progressAnimatorListener != null)
                progressAnimatorListener.onAnimationUpdate((float) animation.getAnimatedValue());
        }
    };

    private final SimpleAnimatorListener endAnimatorListener = new SimpleAnimatorListener() {
        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            if (((float) ((ValueAnimator) animation).getAnimatedValue()) == 0f && progressAnimatorListener != null)
                progressAnimatorListener.onAnimationEnd();
        }
    };

}
