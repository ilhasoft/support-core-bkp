package br.com.ilhasoft.support.core.graphics.transformations;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;

/**
 * Created by daniel on 10/04/16.
 */
public class CircleBitmapTransform implements BitmapTransformation {

    private int diameter;
    private boolean canRecycleSource;

    public CircleBitmapTransform(final int diameter) {
        this(diameter, true);
    }

    public CircleBitmapTransform(final int diameter, final boolean canRecycleSource) {
        this.diameter = diameter;
        this.canRecycleSource = canRecycleSource;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        final Bitmap outputTmp;
        boolean mustRecycleOutputTmp = source.getWidth() != diameter || source.getHeight() != diameter;
        if (mustRecycleOutputTmp) {
            outputTmp = Bitmap.createScaledBitmap(source, diameter, diameter, false);
        } else {
            outputTmp = source;
        }

        final Bitmap output = Bitmap.createBitmap(diameter, diameter, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final float radius = diameter / 2.0f;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, diameter, diameter);

        paint.setDither(true);
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.parseColor("#BAB399")); // yellow
        canvas.drawCircle(radius, radius, radius, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(outputTmp, rect, rect, paint);

        if (mustRecycleOutputTmp) {
            outputTmp.recycle();
        }
        if (canRecycleSource) {
            source.recycle();
        }

        return output;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }

    public void setCanRecycleSource(boolean canRecycleSource) {
        this.canRecycleSource = canRecycleSource;
    }

}
