package br.com.ilhasoft.support.core.widgets;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by daniel on 30/03/16.
 */
public class RecyclerAdapterWrapper<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    private RecyclerView.Adapter<VH> wrappedAdapter;

    public RecyclerAdapterWrapper(RecyclerView.Adapter<VH> adapter) {
        wrappedAdapter = adapter;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return wrappedAdapter.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        wrappedAdapter.onBindViewHolder(holder, position);
    }

    @Override
    public int getItemViewType(int position) {
        return wrappedAdapter.getItemViewType(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
        wrappedAdapter.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return wrappedAdapter.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return wrappedAdapter.getItemCount();
    }

    @Override
    public void onViewRecycled(VH holder) {
        wrappedAdapter.onViewRecycled(holder);
    }

    @Override
    public boolean onFailedToRecycleView(VH holder) {
        return super.onFailedToRecycleView(holder) && wrappedAdapter.onFailedToRecycleView(holder);
    }

    @Override
    public void onViewAttachedToWindow(VH holder) {
        super.onViewAttachedToWindow(holder);
        wrappedAdapter.onViewAttachedToWindow(holder);
    }

    @Override
    public void onViewDetachedFromWindow(VH holder) {
        super.onViewDetachedFromWindow(holder);
        wrappedAdapter.onViewDetachedFromWindow(holder);
    }

    @Override
    public void registerAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        super.registerAdapterDataObserver(observer);
        wrappedAdapter.registerAdapterDataObserver(observer);
    }

    @Override
    public void unregisterAdapterDataObserver(RecyclerView.AdapterDataObserver observer) {
        super.unregisterAdapterDataObserver(observer);
        wrappedAdapter.unregisterAdapterDataObserver(observer);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        wrappedAdapter.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        wrappedAdapter.onDetachedFromRecyclerView(recyclerView);
    }

    public final RecyclerView.Adapter<VH> getWrappedAdapter() {
        return wrappedAdapter;
    }

    public void setWrappedAdapter(RecyclerView.Adapter<VH> wrappedAdapter) {
        this.wrappedAdapter = wrappedAdapter;
        this.notifyDataSetChanged();
    }
}
