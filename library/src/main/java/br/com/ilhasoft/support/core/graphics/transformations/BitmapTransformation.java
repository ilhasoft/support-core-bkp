package br.com.ilhasoft.support.core.graphics.transformations;

import android.graphics.Bitmap;

/**
 * Created by daniel on 10/04/16.
 */
public interface BitmapTransformation {
    /**
    * Transform the source bitmap into a new bitmap. If you create a new bitmap instance, you must
    * call {@link Bitmap#recycle()} on {@code source}. You may return the original
    * if no transformation is required.
    */
    Bitmap transform(Bitmap source);

}
