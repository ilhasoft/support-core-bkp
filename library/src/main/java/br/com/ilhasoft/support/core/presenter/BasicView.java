package br.com.ilhasoft.support.core.presenter;

import android.support.annotation.StringRes;

/**
 * Created by daniel on 04/05/16.
 */
public interface BasicView {

    void showMessage(@StringRes int messageId);

    void showLoading();

    void dismissLoading();

}
