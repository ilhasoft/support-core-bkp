package br.com.ilhasoft.support.core.text;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

/**
 * Class to set a custom Typeface on a spannable.
 *
 * Ref: http://stackoverflow.com/questions/4819049/how-can-i-use-typefacespan-or-stylespan-with-a-custom-typeface
 *
 * Created by daniel on 09/04/16.
 */
public class TypefaceSpan extends MetricAffectingSpan {

    private final Typeface typeface;

    public TypefaceSpan(Typeface typeface) {
        this.typeface = typeface;
    }

    @Override
    public void updateMeasureState(TextPaint textPaint) {
        this.applyCustomTypeFace(textPaint);
    }

    @Override
    public void updateDrawState(TextPaint textPaint) {
        this.applyCustomTypeFace(textPaint);
    }

    private void applyCustomTypeFace(Paint paint) {
        final int oldStyle;
        final Typeface typefaceOld = paint.getTypeface();
        if (typefaceOld != null) {
            oldStyle = typefaceOld.getStyle();
        } else {
            oldStyle = 0;
        }

        final int fake = oldStyle & ~typeface.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }
        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        paint.setTypeface(typeface);
    }

}
