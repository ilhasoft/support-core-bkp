package br.com.ilhasoft.support.core.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import br.com.ilhasoft.support.core.R;

/**
 * Created by daniel on 23/05/16.
 */
public final class DialogHelper {

    private static final String TAG = "DialogHelper";

    private DialogHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static void showQuestion(Context context, @StringRes int titleRes,
                                    @StringRes int messageRes,
                                    DialogInterface.OnClickListener onClickListener) {
        DialogHelper.showQuestion(context, (titleRes > 0) ? context.getString(titleRes) : "",
                                  (messageRes > 0) ? context.getString(messageRes) : "",
                                  onClickListener);
    }

    public static void showQuestion(Context context, String title, String message,
                                    DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setPositiveButton(R.string.yes, onClickListener);
        builder.setNegativeButton(R.string.no, onClickListener);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        if (!TextUtils.isEmpty(message)) {
            builder.setMessage(message);
        }
        builder.show();
    }

}
