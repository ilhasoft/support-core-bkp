package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.support.annotation.ArrayRes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by johncordeiro on 8/12/15.
 */
public final class ArrayHelper {

    private static final String TAG = "ArrayHelper";

    private ArrayHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static List<String> getArrayList(Context context, @ArrayRes int arrayResId) {
        return new ArrayList<>(Arrays.asList(context.getResources().getStringArray(arrayResId)));
    }

    public static Integer[] toObject(int[] intArray) {
        final Integer[] result = new Integer[intArray.length];
        for (int i = 0; i < intArray.length; i++)
            result[i] = intArray[i];
        return result;

    }

    public static int[] toPrimitive(Integer[] integerArray) {
        final int[] result = new int[integerArray.length];
        for (int i = 0; i < integerArray.length; i++)
            result[i] = integerArray[i];
        return result;
    }

    public static Integer[] toInteger(Collection<Integer> collection) {
        int i = 0;
        final Integer[] result = new Integer[collection.size()];
        for (Integer integer : collection) {
            result[i] = integer;
            ++i;
        }
        return result;
    }

    public static int[] toInt(Collection<Integer> collection) {
        int i = 0;
        final int[] result = new int[collection.size()];
        for (Integer integer : collection) {
            result[i] = integer;
            ++i;
        }
        return result;
    }

}
