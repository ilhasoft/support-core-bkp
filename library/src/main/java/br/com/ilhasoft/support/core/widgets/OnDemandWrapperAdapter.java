package br.com.ilhasoft.support.core.widgets;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by daniel on 30/03/16.
 */
public class OnDemandWrapperAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerAdapterWrapper<VH> {

    private boolean enableOnDemand = true;
    private OnDemandListener onDemandListener;

    public OnDemandWrapperAdapter(RecyclerView.Adapter<VH> adapter) {
        this(adapter, null);
    }

    public OnDemandWrapperAdapter(RecyclerView.Adapter<VH> adapter,
                                  @Nullable OnDemandListener onDemandListener) {
        super(adapter);
        this.onDemandListener = onDemandListener;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        super.onBindViewHolder(holder, position);
        if (enableOnDemand && position == this.getItemCount() -1 && onDemandListener != null) {
            onDemandListener.onLoadMore();
        }
    }

    @Nullable
    public OnDemandListener getOnDemandListener() {
        return onDemandListener;
    }

    public void setOnDemandListener(@Nullable OnDemandListener onDemandListener) {
        this.onDemandListener = onDemandListener;
    }

    public boolean isEnableOnDemand() {
        return enableOnDemand;
    }

    public void setEnableOnDemand(boolean enableOnDemand) {
        this.enableOnDemand = enableOnDemand;
    }

}
