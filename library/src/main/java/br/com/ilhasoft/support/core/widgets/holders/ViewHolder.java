package br.com.ilhasoft.support.core.widgets.holders;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by daniel on 03/04/16.
 */
public abstract class ViewHolder<T> extends RecyclerView.ViewHolder {

    private T object;

    public ViewHolder(View itemView) {
        super(itemView);
    }

    public final void bind(T object) {
        this.object = object;
        try {
            this.onBind(object);
        } catch (Exception e) {
            throw new BindViewHolderException(object, e);
        }
    }

    public void start() { }

    public final T getObject() {
        return object;
    }

    public Context getContext() {
        return itemView.getContext();
    }

    public Resources getResources() {
        return itemView.getResources();
    }

    public CharSequence getText(@StringRes int resId) {
        return this.getContext().getText(resId);
    }

    public String getString(@StringRes int resId) {
        return this.getContext().getString(resId);
    }

    public String getString(@StringRes int resId, Object... formatArgs) {
        return this.getContext().getString(resId, formatArgs);
    }

    protected abstract void onBind(T object);

}
