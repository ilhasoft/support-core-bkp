package br.com.ilhasoft.support.core.helpers;

import android.util.Log;

import java.io.Closeable;
import java.io.IOException;

/**
 * Created by daniel on 22/05/16.
 */
public final class CloseableHelper {

    private static final String TAG = "CloseableHelper";

    private CloseableHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                Log.e(TAG, "close: ", e);
            }
        }
    }

}
