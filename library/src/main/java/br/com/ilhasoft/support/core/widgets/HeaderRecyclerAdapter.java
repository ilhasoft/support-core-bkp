package br.com.ilhasoft.support.core.widgets;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by daniel on 02/03/16.
 */
@SuppressWarnings("unchecked")
public class HeaderRecyclerAdapter extends RecyclerAdapterWrapper {

    private static final int TYPE_HEADER = -1;

    private final View viewHeader;

    public HeaderRecyclerAdapter(View viewHeader, RecyclerView.Adapter wrappedAdapter) {
        super(wrappedAdapter);
        this.viewHeader = viewHeader;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return super.getItemViewType(position - 1);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                return new RecyclerView.ViewHolder(viewHeader) { };
            default:
                return super.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position > 0) {
            super.onBindViewHolder(holder, position -1);
        }
    }

}
