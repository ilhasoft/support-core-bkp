package br.com.ilhasoft.support.core.widgets;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by daniel on 04/10/15.
 */
public abstract class FilterableRecyclerAdapter<E, VH extends RecyclerView.ViewHolder>
        extends ListRecyclerAdapter<E, VH> implements Filterable {

    private ListFilter listFilter;
    private List<E> originalObjects;
    private final Object lock = new Object();

    @Override
    public void add(int location, E object) {
        synchronized (lock) {
            if (originalObjects != null)
                originalObjects.add(location, object);
            else
                super.add(location, object);
        }
    }

    @Override
    public boolean add(E object) {
        synchronized (lock) {
            if (originalObjects != null)
                originalObjects.add(object);
            else
                super.add(object);
        }
        return true;
    }

    @Override
    public boolean addAll(int location, Collection<? extends E> collection) {
        synchronized (lock) {
            if (originalObjects != null)
                originalObjects.addAll(location, collection);
            else
                super.addAll(location, collection);
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        synchronized (lock) {
            if (originalObjects != null)
                originalObjects.addAll(collection);
            else
                super.addAll(collection);
        }
        return true;
    }

    @Override
    public void clear() {
        synchronized (lock) {
            if (originalObjects != null)
                originalObjects.clear();
            else
                super.clear();
        }
    }

    @Override
    public E remove(int location) {
        E object;
        synchronized (lock) {
            if (originalObjects != null)
                object = originalObjects.remove(location);
            else
                object = super.remove(location);
        }
        return object;
    }

    @Override
    public boolean remove(Object object) {
        boolean result;
        synchronized (lock) {
            if (originalObjects != null)
                result = originalObjects.remove(object);
            else
                result = super.remove(object);
        }
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean result;
        synchronized (lock) {
            if (originalObjects != null)
                result = originalObjects.removeAll(collection);
            else
                result = super.removeAll(collection);
        }
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        boolean result;
        synchronized (lock) {
            if (originalObjects != null)
                result = originalObjects.retainAll(collection);
            else
                result = super.retainAll(collection);
        }
        return result;
    }

    @Override
    public E set(int location, E object) {
        E oldObject;
        synchronized (lock) {
            if (originalObjects != null)
                oldObject = originalObjects.set(location, object);
            else
                oldObject = super.set(location, object);
        }
        return oldObject;
    }

    @Override
    public void sort(Comparator<? super E> comparator) {
        synchronized (lock) {
            if (originalObjects != null)
                Collections.sort(originalObjects, comparator);
            else
                super.sort(comparator);
        }
    }

    @Override
    public Filter getFilter() {
        if (listFilter == null)
            listFilter = new ListFilter();
        return listFilter;
    }

    protected boolean mustAddToTheResult(final String filterPattern, final E value) {
        return this.mustAddToTheResult(filterPattern, value.toString().toLowerCase());
    }

    protected boolean mustAddToTheResult(final String filterPattern, final String valueText) {
        boolean result = false;

        // First match against the whole, non-splitted value
        if (valueText.startsWith(filterPattern)) {
            result = true;
        } else {
            final String[] words = valueText.split(" ");
            final int wordCount = words.length;

            // Start at index 0, in case valueText starts with space(s)
            for (int i = 0; i < wordCount; i++) {
                if (words[i].startsWith(filterPattern)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    private class ListFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (originalObjects == null) {
                synchronized (lock) {
                    originalObjects = new ArrayList<>(getObjects());
                }
            }

            final ArrayList<E> values = new ArrayList<>();
            if (TextUtils.isEmpty(constraint)) {
                values.addAll(originalObjects);
            } else {
                final int size = originalObjects.size();
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (int i = 0; i < size; i++) {
                    final E value = originalObjects.get(i);
                    if (mustAddToTheResult(filterPattern, value))
                        values.add(value);
                }
            }

            results.values = values;
            results.count = values.size();
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //noinspection unchecked
            setObjects((ArrayList<E>) results.values);
        }
    }

}
