package br.com.ilhasoft.support.core.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;

import br.com.ilhasoft.support.core.databinding.IndeterminateProgressDialogBinding;

/**
 * Created by daniel on 20/03/16.
 */
public class IndeterminateProgressDialog extends AlertDialog {

    private boolean hasStarted;
    private CharSequence message;
    private IndeterminateProgressDialogBinding binding;

    protected IndeterminateProgressDialog(Context context) {
        super(context);
    }

    protected IndeterminateProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    public static IndeterminateProgressDialog show(Context context, int titleId, int messageId) {
        return show(context, titleId, messageId, false);
    }

    public static IndeterminateProgressDialog show(Context context, CharSequence title, CharSequence message) {
        return show(context, title, message, false);
    }

    public static IndeterminateProgressDialog show(Context context, int titleId, int messageId, boolean cancelable) {
        return show(context, titleId, messageId, cancelable, null);
    }

    public static IndeterminateProgressDialog show(Context context, CharSequence title,
                                      CharSequence message, boolean cancelable) {
        return show(context, title, message, cancelable, null);
    }

    public static IndeterminateProgressDialog show(Context context, int titleId, int messageId,
                                      boolean cancelable, OnCancelListener cancelListener) {
        return show(context, getText(context, titleId), getText(context, messageId), cancelable,
                    cancelListener);
    }

    public static IndeterminateProgressDialog show(Context context, CharSequence title,
            CharSequence message, boolean cancelable, OnCancelListener cancelListener) {
        IndeterminateProgressDialog dialog = new IndeterminateProgressDialog(context);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);
        dialog.show();
        return dialog;
    }

    private static CharSequence getText(Context context, int textId) {
        return (textId > 0) ? context.getText(textId) : null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = IndeterminateProgressDialogBinding.inflate(LayoutInflater.from(this.getContext()), null, false);
        this.setView(binding.getRoot());
        if (message != null) {
            this.setMessage(message);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        hasStarted = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        hasStarted = false;
    }

    @Override
    public void setMessage(CharSequence message) {
        if (binding != null) {
            binding.message.setText(message);
        } else {
            this.message = message;
        }
    }

}
