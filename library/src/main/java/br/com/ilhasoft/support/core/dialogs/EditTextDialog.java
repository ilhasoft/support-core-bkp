package br.com.ilhasoft.support.core.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.widget.EditText;

import br.com.ilhasoft.support.core.helpers.DimensionHelper;

/**
 * Created by daniel on 11/04/16.
 */
public class EditTextDialog extends AlertDialog {

    private EditText editText;
    private CharSequence title;
    private CharSequence hintText;
    private CharSequence positiveText;
    private CharSequence negativeText;
    private OnInputTextListener onInputTextListener;

    public static EditTextDialog show(Context context, @StringRes int title,
                                      @StringRes int hintText,
                                      @StringRes int positiveText,
                                      @StringRes int negativeText,
                                      OnInputTextListener onInputTextListener) {
        return EditTextDialog.show(context, getText(context, title), getText(context, hintText),
                                   getText(context, positiveText), getText(context, negativeText),
                                   onInputTextListener);
    }

    public static EditTextDialog show(Context context, CharSequence title, CharSequence hintText,
                                      CharSequence positiveText, CharSequence negativeText,
                                      OnInputTextListener onInputTextListener) {
        EditTextDialog editTextDialog = new EditTextDialog(context);
        editTextDialog.title = title;
        editTextDialog.hintText = hintText;
        editTextDialog.positiveText = positiveText;
        editTextDialog.negativeText = negativeText;
        editTextDialog.onInputTextListener = onInputTextListener;
        editTextDialog.setCancelable(true);
        editTextDialog.show();
        return editTextDialog;
    }

    private static CharSequence getText(Context context, int textId) {
        return (textId > 0) ? context.getText(textId) : null;
    }

    protected EditTextDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final Context context = this.getContext();
        editText = new AppCompatEditText(context);
        editText.setHint(hintText);

        final int spacing = Math.round(DimensionHelper.toPx(context, 16));
        this.setView(editText, spacing, spacing / 2, spacing, spacing / 2);
        this.setTitle(title);
        this.setButton(BUTTON_POSITIVE, positiveText, onClick);
        this.setButton(BUTTON_NEGATIVE, negativeText, onClick);
        super.onCreate(savedInstanceState);
    }

    private final OnClickListener onClick = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (onInputTextListener != null && which == BUTTON_POSITIVE) {
                onInputTextListener.onInputText(dialog, editText.getText());
            }
        }
    };

    public interface OnInputTextListener {
        void onInputText(DialogInterface dialog, CharSequence text);
    }

}
