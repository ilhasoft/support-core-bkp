package br.com.ilhasoft.support.core.listeners;

/**
 * Created by danielsan on 5/9/16.
 */
public interface OnClickListener<T> {

    void onClick(T t);

}
