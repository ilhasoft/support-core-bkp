package br.com.ilhasoft.support.core.widgets.holders;

import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;

/**
 * Created by daniel on 03/04/16.
 */
public class BindViewHolderException extends RuntimeException {

    BindViewHolderException(Object object, Throwable throwable) {
        super("Exception occurred while trying to bind the object: " + String.valueOf(object), throwable);
    }

    @Override
    public void printStackTrace(PrintStream err) {
        try {
            this.printStackTrace(err, "");
        } catch (IOException e) {
            // Appendable.append throws IOException, but PrintWriter.append doesn't.
            throw new AssertionError();
        }
    }

    @Override
    public void printStackTrace(PrintWriter err) {
        try {
            this.printStackTrace(err, "");
        } catch (IOException e) {
            // Appendable.append throws IOException, but PrintWriter.append doesn't.
            throw new AssertionError();
        }
    }

    private void printStackTrace(Appendable err, String indent)
            throws IOException {
        err.append(this.toString());
        err.append("\n");

        StackTraceElement[] stack = this.getStackTrace();
        if (stack != null) {
            final int maxPrints = Math.min(stack.length, 10);
            for (int i = 0; i < maxPrints; i++) {
                err.append(indent);
                err.append("\tat ");
                err.append(stack[i].toString());
                err.append("\n");
            }

            if (maxPrints > 0) {
                err.append(indent);
                err.append("\t... ");
                err.append(Integer.toString(stack.length - maxPrints));
                err.append(" more\n");
            }
        }

        final Throwable cause = getCause();
        if (cause != null) {
            err.append(indent);
            err.append("Caused by: ");
            if (err instanceof PrintStream) {
                cause.printStackTrace((PrintStream) err);
            } else if (err instanceof PrintWriter) {
                cause.printStackTrace((PrintWriter) err);
            }
        }
    }

}
