package br.com.ilhasoft.support.core.helpers;

import android.support.annotation.Nullable;

/**
 * Created by daniel on 22/05/16.
 */
public class ObjectHelper {

    private static final String TAG = "ObjectHelper";

    private ObjectHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static boolean equal(@Nullable Object a, @Nullable Object b) {
        return a == b || a != null && a.equals(b);
    }

}
