package br.com.ilhasoft.support.core.app;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatDialog;

/**
 * Created by daniel on 23/05/16.
 */
public class DialogFragment extends android.support.v4.app.DialogFragment {

    @NonNull
    @Override
    public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {
        return new AppCompatDialog(this.getActivity(), this.getTheme());
    }

    @Override
    public AppCompatDialog getDialog() {
        return (AppCompatDialog) super.getDialog();
    }

}
