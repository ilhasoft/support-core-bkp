package br.com.ilhasoft.support.core.graphics.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.Element;
import android.support.v8.renderscript.RenderScript;
import android.support.v8.renderscript.ScriptIntrinsicBlur;

import br.com.ilhasoft.support.core.graphics.BitmapHelper;

/**
 * Created by daniel on 10/04/16.
 */
public class BlurEffectBitmapTransformation implements BitmapTransformation {

    private static final float MAX_RADIUS_VALUE = 25f;

    private final float radius;
    private final RenderScript renderScript;
    private final boolean createCopyBitmap;

    public BlurEffectBitmapTransformation(final Context context) {
        this(context, MAX_RADIUS_VALUE, false);
    }

    public BlurEffectBitmapTransformation(final Context context, final float radius) {
        this(context, radius, false);
    }

    public BlurEffectBitmapTransformation(final Context context, final float radius,
                                          final boolean createCopyBitmap) {
        this.radius = radius;
        this.createCopyBitmap = createCopyBitmap;
        this.renderScript = RenderScript.create(context);
    }

    @Override
    public Bitmap transform(Bitmap source) {
        final Bitmap output;
        if (createCopyBitmap) {
            output = source.copy(source.getConfig(), true);
        } else {
            output = Bitmap.createBitmap(source);
        }

        final Allocation tmpIn = Allocation.createFromBitmap(renderScript, source);
        final Allocation tmpOut = Allocation.createFromBitmap(renderScript, output);

        //Intrinsic Gausian blur filter
        final ScriptIntrinsicBlur scriptIntrinsicBlur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        scriptIntrinsicBlur.setRadius(radius);
        scriptIntrinsicBlur.setInput(tmpIn);
        scriptIntrinsicBlur.forEach(tmpOut);
        tmpOut.copyTo(output);

        if (!source.equals(output)) {
            BitmapHelper.recycle(source);
        }

        renderScript.destroy();

        return output;
    }

}
