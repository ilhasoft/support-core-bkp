package br.com.ilhasoft.support.core.graphics;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import br.com.ilhasoft.support.core.helpers.CloseableHelper;

/**
 * Created by danielsan on 8/20/15.
 */
public final class BitmapHelper {

    private static final String TAG = "BitmapHelper";

    private BitmapHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @Nullable
    public static Bitmap getBitmapFromUri(Context context, Uri uri) {
        try {
            return BitmapHelper.getBitmapFromInputStream(context.getContentResolver().openInputStream(uri));
        } catch (FileNotFoundException e) {
            Log.e(TAG, "getBitmapFromUri: ", e);
        }

        return null;
    }

    @Nullable
    public static Bitmap getBitmapFromInputStream(InputStream inputStream) {
        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
        if (bitmap != null) {
            CloseableHelper.close(inputStream);
        }
        return bitmap;
    }

    @Nullable
    public static Bitmap getScaledBitmapFromUri(Context context, Uri uri, int targetLength) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);

            BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();
            bitmapFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, bitmapFactoryOptions);
            int scaleFactor = Math.min(bitmapFactoryOptions.outWidth / targetLength,
                                       bitmapFactoryOptions.outHeight / targetLength);
            bitmapFactoryOptions.inJustDecodeBounds = false;
            bitmapFactoryOptions.inSampleSize = scaleFactor;
            bitmapFactoryOptions.inPurgeable = true;

            CloseableHelper.close(inputStream);
            inputStream = context.getContentResolver().openInputStream(uri);
            Bitmap bitmapOutput = BitmapFactory.decodeStream(inputStream, null, bitmapFactoryOptions);
            CloseableHelper.close(inputStream);

            return bitmapOutput;
        } catch (Exception e) {
            Log.e(TAG, "getScaledBitmapFromUri: ", e);
        }

        return null;
    }

    @Nullable
    public static byte[] getBytesFromBitmap(Bitmap bitmap, int targetSize) {
        int quality = 100;
        while (quality >= 50) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
                int size = byteArrayOutputStream.size() / 1024;
                if (size > targetSize && quality >= 60) {
                    quality -= 10;
                    continue;
                }
                return byteArrayOutputStream.toByteArray();
            } catch (Exception e) {
                Log.e(TAG, "getBytesFromBitmap: ", e);
            }
            quality -= 10;
        }

        return null;
    }

    @Nullable
    public static Bitmap getScaledBitmapFromFile(String path, int targetLength) {
        try {
            BitmapFactory.Options bitmapFactoryOptions = new BitmapFactory.Options();
            bitmapFactoryOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, bitmapFactoryOptions);
            int scaleFactor = Math.min(bitmapFactoryOptions.outWidth / targetLength,
                                       bitmapFactoryOptions.outHeight / targetLength);
            bitmapFactoryOptions.inJustDecodeBounds = false;
            bitmapFactoryOptions.inSampleSize = scaleFactor;
            bitmapFactoryOptions.inPurgeable = true;
            return BitmapFactory.decodeFile(path, bitmapFactoryOptions);
        } catch (Exception e) {
            Log.e(TAG, "getScaledBitmapFromFile: ", e);
        }

        return null;
    }

    public static void recycle(final Bitmap bitmap) {
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }
    }

}
