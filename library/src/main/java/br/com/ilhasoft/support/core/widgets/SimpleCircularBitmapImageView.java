package br.com.ilhasoft.support.core.widgets;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import br.com.ilhasoft.support.core.graphics.transformations.CircleBitmapTransform;
import br.com.ilhasoft.support.core.helpers.ObjectHelper;

/**
 * Created by danielsan on 11/9/15.
 */
public class SimpleCircularBitmapImageView extends AppCompatImageView {

    private Bitmap circleBitmap;
    private int lastDiameter = -1;
    private Bitmap lastBitmap = null;
    private CircleBitmapTransform circleBitmapTransform;

    public SimpleCircularBitmapImageView(Context context) {
        super(context);
        circleBitmapTransform = new CircleBitmapTransform(lastDiameter, false);
    }

    public SimpleCircularBitmapImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        circleBitmapTransform = new CircleBitmapTransform(lastDiameter, false);
    }

    public SimpleCircularBitmapImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        circleBitmapTransform = new CircleBitmapTransform(lastDiameter, false);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final Drawable drawable = this.getDrawable();
        final int diameter = Math.min(this.getWidth(), this.getHeight());
        if (drawable == null || !(drawable instanceof BitmapDrawable) || diameter == 0) {
            super.onDraw(canvas);
        } else {
            final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            if (bitmap == null) {
                super.onDraw(canvas);
            } else {
                if (!ObjectHelper.equal(bitmap, lastBitmap) || lastDiameter != diameter) {
                    lastBitmap = bitmap;
                    lastDiameter = diameter;
                    circleBitmapTransform.setDiameter(diameter);
                    circleBitmap = circleBitmapTransform.transform(bitmap);
                }
                canvas.drawBitmap(circleBitmap, 0, 0, null);
            }
        }
    }

}
