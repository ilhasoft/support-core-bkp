package br.com.ilhasoft.support.core.widgets;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by daniel on 04/10/15.
 */
public abstract class ListRecyclerAdapter<E, VH extends RecyclerView.ViewHolder>
        extends EmptyRecyclerAdapter<VH> implements List<E> {

    private List<E> objects;

    public ListRecyclerAdapter() {
        super();
        objects = new ArrayList<>();
    }

    public ListRecyclerAdapter(int capacity) {
        super();
        objects = new ArrayList<>(capacity);
    }

    public ListRecyclerAdapter(Collection<? extends E> collection) {
        super();
        objects = new ArrayList<>(collection);
    }

    @Override
    public long getItemId(int position) {
        if (objects.size() >= position)
            return objects.get(position).hashCode();
        return super.getItemId(position);
    }

    @Override
    public int getItemCount() {
        return this.size();
    }

    /******************************** List implementation ***********************************/

    @Override
    public void add(int location, E object) {
        objects.add(location, object);
        this.notifyItemInserted(location);
    }

    @Override
    public boolean add(E object) {
        objects.add(object);
        this.notifyItemInserted(this.size() - 1);
        return true;
    }

    @Override
    public boolean addAll(int location, Collection<? extends E> collection) {
        objects.addAll(location, collection);
        this.notifyItemRangeInserted(location, collection.size());
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        int positionStart = this.size();
        objects.addAll(collection);
        this.notifyItemRangeInserted(positionStart, collection.size());
        return true;
    }

    @Override
    public void clear() {
        objects.clear();
        this.notifyDataSetChanged();
    }

    @Override
    public boolean contains(Object object) {
        return objects.contains(object);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return objects.containsAll(collection);
    }

    @Override
    public E get(int location) {
        return objects.get(location);
    }

    @Override
    public int indexOf(Object object) {
        return objects.indexOf(object);
    }

    @Override
    public boolean isEmpty() {
        return objects.isEmpty();
    }

    @NonNull
    @Override
    public Iterator<E> iterator() {
        return objects.iterator();
    }

    @Override
    public int lastIndexOf(Object object) {
        return objects.lastIndexOf(object);
    }

    @Override
    public ListIterator<E> listIterator() {
        return objects.listIterator();
    }

    @NonNull
    @Override
    public ListIterator<E> listIterator(int location) {
        return objects.listIterator(location);
    }

    @Override
    public E remove(int location) {
        E object = objects.remove(location);
        this.notifyItemRemoved(location);
        return object;
    }

    @Override
    public boolean remove(Object object) {
        int index = objects.indexOf(object);
        boolean result = objects.remove(object);
        if (index != -1)
            this.notifyItemRemoved(index);
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        boolean result = objects.removeAll(collection);
        if (result)
            this.notifyDataSetChanged();
        return result;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        boolean result = objects.retainAll(collection);
        if (result)
            this.notifyDataSetChanged();
        return result;
    }

    @Override
    public E set(int location, E object) {
        E oldObject = objects.set(location, object);
        this.notifyItemChanged(location);
        return oldObject;
    }

    @Override
    public int size() {
        return objects.size();
    }

    @NonNull
    @Override
    public List<E> subList(int start, int end) {
        return objects.subList(start, end);
    }

    @NonNull
    @Override
    public Object[] toArray() {
        return objects.toArray();
    }

    @NonNull
    @Override
    public <T> T[] toArray(T[] array) {
        return objects.toArray(array);
    }

    public void sort(Comparator<? super E> comparator) {
        Collections.sort(objects, comparator);
        this.notifyDataSetChanged();
    }

    protected List<E> getObjects() {
        return objects;
    }

    protected void setObjects(ArrayList<E> objects) {
        this.objects = objects;
        this.notifyDataSetChanged();
    }

}
