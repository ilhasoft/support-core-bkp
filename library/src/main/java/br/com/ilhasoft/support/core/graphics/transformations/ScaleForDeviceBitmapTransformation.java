package br.com.ilhasoft.support.core.graphics.transformations;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.view.WindowManager;

/**
 * Created by daniel on 25/04/16.
 */
public class ScaleForDeviceBitmapTransformation implements BitmapTransformation {

    private final int maxWidth;
    private final int maxHeight;

    public ScaleForDeviceBitmapTransformation(Context context) {
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Point point = new Point();
        windowManager.getDefaultDisplay().getSize(point);
        maxWidth = point.x * 2;
        maxHeight = point.y * 2;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        final int sourceWidth = source.getWidth();
        final int sourceHeight = source.getHeight();
        // source is probably less than the maximum texture size
        if (sourceWidth < (maxWidth * 1.5) && sourceHeight < (maxHeight * 1.5)) {
            return source;
        }

        final int targetWidth;
        final int targetHeight;
        final double aspectRatio;

        if (source.getWidth() > source.getHeight()) {
            targetWidth = maxWidth;
            aspectRatio = (double) source.getHeight() / (double) source.getWidth();
            targetHeight = (int) Math.round(targetWidth * aspectRatio);
        } else {
            targetHeight = maxHeight;
            aspectRatio = (double) source.getWidth() / (double) source.getHeight();
            targetWidth = (int) Math.round(targetHeight * aspectRatio);
        }

        Bitmap output = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
        if (output != source) {
            source.recycle();
        }

        return output;
    }

}
