package br.com.ilhasoft.support.core.widgets;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by daniel on 04/05/16.
 */
public abstract class WidgetPagerAdapter extends PagerAdapter {

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object.equals(view);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = this.onCreateView(container, position);
        container.addView(view);
        this.onViewCreated(view, position);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    protected abstract View onCreateView(ViewGroup parent, int position);

    protected abstract void onViewCreated(View view, int position);

}
