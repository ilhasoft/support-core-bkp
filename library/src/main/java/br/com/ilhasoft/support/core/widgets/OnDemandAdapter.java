package br.com.ilhasoft.support.core.widgets;

import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

/**
 * Created by daniel on 30/03/16.
 */
public abstract class OnDemandAdapter<E, VH extends RecyclerView.ViewHolder> extends FilterableRecyclerAdapter<E, VH> {

    private boolean enableOnDemand = true;
    private OnDemandListener onDemandListener;

    public OnDemandAdapter() {
        this(null);
    }

    public OnDemandAdapter(@Nullable OnDemandListener onDemandListener) {
        this.onDemandListener = onDemandListener;
    }

    @Override
    @CallSuper
    public void onBindViewHolder(VH holder, int position) {
        if (enableOnDemand && position == this.getItemCount() -1 && onDemandListener != null) {
            onDemandListener.onLoadMore();
        }
    }

    @Nullable
    public OnDemandListener getOnDemandListener() {
        return onDemandListener;
    }

    public void setOnDemandListener(@Nullable OnDemandListener onDemandListener) {
        this.onDemandListener = onDemandListener;
    }

    public boolean isEnableOnDemand() {
        return enableOnDemand;
    }

    public void setEnableOnDemand(boolean enableOnDemand) {
        this.enableOnDemand = enableOnDemand;
    }

}
