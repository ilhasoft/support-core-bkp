package br.com.ilhasoft.support.core.animation;

import android.animation.Animator;

/**
 * Created by daniel on 29/11/15.
 */
public class SimpleAnimatorListener implements Animator.AnimatorListener {

    @Override
    public void onAnimationStart(Animator animation) { }

    @Override
    public void onAnimationEnd(Animator animation) { }

    @Override
    public void onAnimationCancel(Animator animation) { }

    @Override
    public void onAnimationRepeat(Animator animation) { }

}
