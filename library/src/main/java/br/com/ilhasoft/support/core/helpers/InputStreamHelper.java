package br.com.ilhasoft.support.core.helpers;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by danielsan on 9/15/15.
 */
public final class InputStreamHelper {

    private static final String TAG = "InputStreamHelper";

    private InputStreamHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    /**
     * Returns the contents of this stream as a string.
     *
     * @param inputStream the {@code InputStream} to get the contents
     * @return the string representation of the data in this stream.
     */
    public static String toString(InputStream inputStream) {
        String line;
        BufferedReader bufferedReader = null;
        final StringBuilder stringBuilder = new StringBuilder();
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            Log.e(TAG, "toString: ", e);
        } finally {
            CloseableHelper.close(inputStream);
            CloseableHelper.close(bufferedReader);
        }

        return stringBuilder.toString();
    }

}
