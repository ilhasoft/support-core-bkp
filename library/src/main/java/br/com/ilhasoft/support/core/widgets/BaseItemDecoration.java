package br.com.ilhasoft.support.core.widgets;

import android.support.annotation.IntDef;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class BaseItemDecoration extends RecyclerView.ItemDecoration {

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ LinearLayoutManager.HORIZONTAL, LinearLayoutManager.VERTICAL})
    public @interface Orientation {}

    @Orientation
    protected int orientation;

    public BaseItemDecoration(@Orientation int orientation) {
        this.orientation = orientation;
    }

    @Orientation
    public int getOrientation() {
        return orientation;
    }

    public void setOrientation(@Orientation int orientation) {
        if (orientation != LinearLayoutManager.HORIZONTAL
                && orientation != LinearLayoutManager.VERTICAL) {
            throw new IllegalArgumentException("Invalid orientation.");
        }
        this.orientation = orientation;
    }

}
