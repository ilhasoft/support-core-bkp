package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by danielsan on 8/20/15.
 */
public final class FileHelper {

    private static final String TAG = "FileHelper";

    private FileHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    @Nullable
    public static File generateTempFile(Context contex, String prefix, String suffix) {
        String fileName = String.format("%s_%s_", prefix, (new SimpleDateFormat("yyyyMMdd_HHmmss")).format(new Date()));
        File storageDir = contex.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
//        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        try {
            return File.createTempFile(fileName, suffix, storageDir);
        } catch (IOException e) {
            Log.e(TAG, "generateTempFile: ", e);
            return null;
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static byte[] fromFile(File file) {
        try {
            int size = (int) file.length();
            byte[] bytes = new byte[size];

            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
            bufferedInputStream.read(bytes, 0, bytes.length);
            bufferedInputStream.close();

            return bytes;
        } catch (FileNotFoundException e) {
            Log.e(TAG, "fromFile: ", e);
        } catch (IOException e) {
            Log.e(TAG, "fromFile: ", e);
        }

        return new byte[0];
    }

}
