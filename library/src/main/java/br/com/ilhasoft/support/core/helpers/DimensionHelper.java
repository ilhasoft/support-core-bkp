package br.com.ilhasoft.support.core.helpers;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;

/**
 * Created by daniel on 23/11/15.
 */
public final class DimensionHelper {

    private static final String TAG = "DimensionHelper";

    private DimensionHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    public static float toPx(Context context, float dpValue) {
        return DimensionHelper.toPx(context.getResources(), dpValue);
    }

    public static float toPx(Resources resources, float dpValue) {
        return DimensionHelper.toPx(resources.getDisplayMetrics(), dpValue);
    }

    public static float toPx(DisplayMetrics displayMetrics, float dpValue) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValue, displayMetrics);
    }

}
